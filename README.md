# mirror

## Setup
This script downloads all packages in parallel and is much faster than the update script when you don't have a local copy yet.
```bash
./setup.sh
```

## Updating
```bash
./update.sh
```

## Serving
```bash
./serve.sh
```
